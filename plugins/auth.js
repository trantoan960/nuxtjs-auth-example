export default function ({ $auth, app }) {
  if ($auth && !$auth.loggedIn) {
    return false
  }
}
